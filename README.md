# Application links in Jira

This is a tutorial for utilising Application Links within a JIRA plugin.

For more details, go to: [Application links in Jira][1].

## Running locally

To run this app locally, make sure that you have the Atlassian Plugin SDK installed, and then run:

    atlas-mvn jira:run

 [1]: https://developer.atlassian.com/display/JIRADEV/Tutorial+-+Application+links+in+JIRA
