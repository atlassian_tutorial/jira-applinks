package com.example.plugins.tutorial.jira.tabpanels;

import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.ApplicationLinkResponseHandler;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.EntityLink;
import com.atlassian.applinks.api.EntityLinkService;
import com.atlassian.applinks.api.application.confluence.ConfluenceSpaceEntityType;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.tabpanels.GenericMessageAction;
import com.atlassian.jira.plugin.issuetabpanel.AbstractIssueTabPanel;
import com.atlassian.jira.plugin.issuetabpanel.IssueAction;
import com.atlassian.jira.plugin.issuetabpanel.IssueTabPanel;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.http.JiraUrl;
import com.atlassian.jira.web.ExecutingHttpRequest;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ConfluenceSpaceTabPanel extends AbstractIssueTabPanel implements IssueTabPanel
{
    private static final Logger log = LoggerFactory.getLogger(ConfluenceSpaceTabPanel.class);

    private final EntityLinkService entityLinkService;

    public ConfluenceSpaceTabPanel(EntityLinkService entityLinkService)
    {
        this.entityLinkService = entityLinkService;
    }

    public List getActions(Issue issue, ApplicationUser remoteUser)
    {
        EntityLink entityLink = entityLinkService.getPrimaryEntityLink(issue.getProjectObject(), ConfluenceSpaceEntityType.class);
        if (entityLink == null)
        {
            return Collections.singletonList(new GenericMessageAction("No Link to a Confluence for this JIRA Project configured"));
        }
        ApplicationLinkRequestFactory requestFactory = entityLink.getApplicationLink().createAuthenticatedRequestFactory();

        final String query = issue.getKey();
        String confluenceContentType = "page";
        final String spaceKey = entityLink.getKey();

        try
        {
            // Create a request to Confluence's REST API looking for our issue
            ApplicationLinkRequest request = requestFactory.createRequest(Request.MethodType.GET, "/rest/prototype/1/search?query=" + query + "&spaceKey=" + spaceKey + "&type=" + confluenceContentType);
            String responseBody = request.execute(new ApplicationLinkResponseHandler<String>()
            {
                public String credentialsRequired(final Response response) throws ResponseException
                {
                    return response.getResponseBodyAsString();
                }

                public String handle(final Response response) throws ResponseException
                {
                    return response.getResponseBodyAsString();
                }
            });

            // Parse the resulting REST response
            Document document = parseResponse(responseBody);
            NodeList results = document.getDocumentElement().getChildNodes();

            // List of items we will be showing in the tab panel.
            List<IssueAction> issueActions = new ArrayList<IssueAction>();

            for (int j = 0; j < results.getLength(); j++)
            {
                NodeList links = results.item(j).getChildNodes();
                for (int i = 0; i < links.getLength(); i++)
                {
                    Node linkNode = links.item(i);
                    if ("link".equals(linkNode.getNodeName()))
                    {
                        NamedNodeMap attributes = linkNode.getAttributes();
                        Node type = attributes.getNamedItem("type");
                        if (type != null && "text/html".equals(type.getNodeValue()))
                        {
                            Node href = attributes.getNamedItem("href");
                            URI uriToConfluencePage = URI.create(href.getNodeValue());
                            // TODO Should abstract this out into it's own IssueAction and use a templating language for this HTML generation
                            // To avoid nasty HTML building within Java
                            // This is beyond the scope of the tutorial, and is left as an exercise for the reader
                            IssueAction searchResult = new GenericMessageAction(String.format("Reference to Issue found in Confluence page <a target=\"_new\" href=%1$s>%1$s</a>", uriToConfluencePage.toString()));
                            issueActions.add(searchResult);
                        }
                    }
                }
            }
            return issueActions;
        }
        catch (CredentialsRequiredException e)
        {
            // Authorisation needs to be given to access Confluence as this user via UAL
            // TODO Should abstract this out into it's own IssueAction and use a templating language for this HTML generation
            // To avoid nasty HTML building within Java
            // This is beyond the scope of the tutorial, and is left as an exercise for the reader
            final HttpServletRequest req = ExecutingHttpRequest.get();
            URI authorisationURI = e.getAuthorisationURI(URI.create(JiraUrl.constructBaseUrl(req) + "/browse/" + issue.getKey()));
            String message = "You have to authorise this operation first. <a target=\"_new\" href=%s>Please click here and login into the remote application.</a>";
            IssueAction credentialsRequired = new GenericMessageAction(String.format(message, authorisationURI));
            return Collections.singletonList(credentialsRequired);
        }
        catch (ResponseException e)
        {
            // Exception thrown from Confluence's REST API
            return Collections.singletonList(new GenericMessageAction("Response exception. Message: " + e.getMessage()));
        }
        catch (ParserConfigurationException e)
        {
            // Our parsing of the REST response has failed - bug in this tutorial or the REST response shape has changed
            return Collections.singletonList(new GenericMessageAction("Failed to read response from Confluence." + e.getMessage()));
        }
        catch (SAXException e)
        {
            // Our parsing of the REST response has failed - bug in this tutorial or the REST response shape has changed
            return Collections.singletonList(new GenericMessageAction("Failed to read response from Confluence." + e.getMessage()));
        }
        catch (IOException e)
        {
            // Our parsing of the REST response has failed - bug in this tutorial or the REST response shape has changed
            return Collections.singletonList(new GenericMessageAction("Failed to read response from Confluence." + e.getMessage()));
        }
    }

    public boolean showPanel(Issue issue, ApplicationUser remoteUser)
    {
        return true;
    }

    private Document parseResponse(String body) throws ParserConfigurationException, IOException, SAXException
    {
        // Build a Document for this response for us to walk
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        InputStream is = new ByteArrayInputStream(body.getBytes("UTF-8"));
        return db.parse(is);
    }
}
